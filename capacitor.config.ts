import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.redbeard.oracle',
  appName: 'oracle-android',
  webDir: 'public',
  bundledWebRuntime: false
};

export default config;
