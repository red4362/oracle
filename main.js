var express = require('express');
var app = express();
var fs = require('fs');
var path = require('path');
var cors =require('cors');

app.use(cors());

app.use(
   express.urlencoded({
      extended: true
   })
);

var isFile = (fileName) => {
   return fs.lstatSync(fileName).isFile();
}

app.use(express.json());

app.use(
   express.static('images', {})
);
app.use(
   express.static('css', {})
);
app.use(
   express.static('js', {})
);
app.use(
   express.static(__dirname, {index: "index.html"})
);
app.get('/sci', (req, res) => {
   var imgobj = [];
   var images = fs.readdirSync('./images/gma/sci').map((fileName) => {
      return path.join('./images/gma/sci', fileName);
   })
   .filter(isFile);
   for(var i = 0; i < images.length; i++)
   {
     imgobj.push({image: images[i]});
   }
   res.json({imgobj});
});
app.get('/base', (req, res) => {
   var imgobj = [];
   var images = fs.readdirSync('./images/gma/base').map((fileName) => {
      return path.join('./images/gma/base', fileName);
   })
   .filter(isFile);
   for(var s = 0; s < images.length; s++)
   {
      imgobj.push({image: images[s]}); 
   }
   res.json({imgobj});
});

app.listen(8082);
