var baseimgs;
var sciimgs;
var instance;
var fudgetypes = [{dietype: "/images/fudge-blank.svg"}, {dietype: "/images/fudge-minus.svg"}, {dietype: "/images/fudge-plus.svg"}]
var app = new Vue(
{
   el: '#app',
   data: {
      imagelist: [],
      typeofcards: 'Sci-Fi',
      numofcards: 0,
      imgbool: false,
      numoffudge: 0,
      fudgelist: [],
      fudgebool: false,
   },
   mounted: function() {
     instance = this;
     axios.get('/base')
     .then((res) => {
        baseimgs = res.data.imgobj;
     })
     .catch((err) => {
        console.log("Could not retrieve base image list: " + err);
     });
     axios.get('/sci')
     .then((res) => {
       sciimgs = res.data.imgobj;
     })
     .catch((err)=> { console.log('There was an error: ' + err); });
   },
   methods: {
     gencard: function() {
       var random = new Random();
       instance.imagelist = [];
       if(instance.typeofcards === 'Sci-Fi')
       {
          _.forEach(_.range(instance.numofcards), function() {
             instance.imagelist.push(sciimgs[random.integer(0,119)].image);
          });
          instance.imgbool = true;
       }
       if(instance.typeofcards == 'Base')
       {
          var random = new Random();
          instance.imagelist = [];
          _.forEach(_.range(instance.numofcards), function() {
             instance.imagelist.push(baseimgs[random.integer(0,119)].image);
          });
          instance.imgbool = true;
       }
     },
     genfudge: function() {
       var dice = [];
       var random = new Random();
       instance.fudgelist = [];
       _.forEach(_.range(instance.numoffudge), function() {
         instance.fudgelist.push(fudgetypes[random.integer(0,2)]);
       });
       instance.fudgebool = true;
     }
   }
})
